const fs = require('fs');
const path = require('path');
const employees = require('./data.json').employees;

const dataFileName = 'data.json';
const DATAFILE = path.join(__dirname, 'data.dataFileName');

const generateJSONfile = (fileName, data, callback) => {
    if (fileName === undefined || data === undefined || callback === undefined || typeof fileName !== 'string' || typeof callback !== 'function') {
        console.error(`Function arguments must be (Data<JSON values>, function<callback function>)`);
    } else {
        try {
            const output = JSON.stringify(data, null, 4);
            const filePath = path.join(__dirname, fileName);
            fs.writeFile(filePath, output, (error) => {
                if (error) {
                    callback(error);
                } else {
                    callback(null, filePath);
                }
            });

        } catch (error) {
            callback(error);
        }
    }
}

const retrieveDataById = (id, callback) => {
    if (id === undefined || callback === undefined || typeof id !== 'number' || typeof callback !== 'function') {
        const error = 'Function arguments must be (id<number>, function<callback function>)';
        console.error(error);
    } else {
        const employeeInfo = employees.filter((employee) => {
            return employee.id === id;
        });
        if (employeeInfo.length === 0) {
            const error = `Employee NOT FOUND with employee id: ${id}`;
            callback(error);
        } else {
            callback(null, employeeInfo[0]);
        }
    }
}

const retrieveDataByIds = (ids, callback) => {
    if (ids === undefined || !Array.isArray(ids) || callback === undefined || typeof callback !== 'function') {
        const error = 'Function arguments must be (Array<id numbers>, function<callback function>)';
        console.error(error);
    } else {
        const employeeInfos = [];
        for (const id of ids) {
            retrieveDataById(id, (error, employeeInfo) => {
                if (error) {
                    callback(err);
                } else {
                    employeeInfos.push(employeeInfo);
                }
                if (employeeInfos.length === ids.length) {
                    console.log(`ALL DATA RETREIVED SUCCESSFULLY`);
                    callback(null, employeeInfos);
                }
            });
        }
    }
}

const groupByCompanies = (employees, callback) => {
    if (employees === undefined || !Array.isArray(employees) || callback===undefined || typeof callback!=='function') {
        console.log(`Employees data <Array> must be passed as argument`);
    } else {
        const employeesGroupedByCompany = employees.reduce((groupedbyCompany, employee) => {
            const company = employee.company;
            groupedbyCompany[company] = (groupedbyCompany[company] || []).concat(employee);
            return groupedbyCompany;
        }, {});

        if (Object.keys(employeesGroupedByCompany).length === 0) {
            const error = `Employees Info couln't be grouped`;
            callback(error);
        } else {
            callback(null, employeesGroupedByCompany);
        }
    }

}

const employeesIds = [2, 13, 23];
retrieveDataByIds(employeesIds, (error, employeesInfo) => {
    if (error) {
        console.error(error);
    } else {
        const fileName = 'employeeInfoByIds.json';
        generateJSONfile(fileName, employeesInfo, (error, filePath) => {
            if (error) {
                console.error(error);
            } else {
                console.log(`JSON file for EMPLOYEES BY ID Generated with file path:`, filePath);
                groupByCompanies(employees, (error, groupedEmployees) => {
                    if (error) {
                        console.error(error);
                    } else {
                        const groupedFileName = 'grouped_by_company.json';
                        generateJSONfile(groupedFileName, groupedEmployees, (error, filePath) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log(`JSON file for group by company generated with file path:`, filePath);
                            }
                        });
                    }


                })

            }
        })
    }
});
